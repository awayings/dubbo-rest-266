package com.clubfactory.platform.dubbo.rest266.client.service;

/**
 * @author jingzhang@clubfactory.com
 * @time 2018-2-28 09:13:52
 */
public interface DubboRestService {

    String echo(String name);
}
