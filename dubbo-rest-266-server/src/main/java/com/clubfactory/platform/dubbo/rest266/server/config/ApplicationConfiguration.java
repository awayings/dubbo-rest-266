package com.clubfactory.platform.dubbo.rest266.server.config;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * @author lianghaijun
 * @date 2018-11-20
 */
@Configuration
//@EnableDubbo(basePackages = "com.clubfactory.platform.dubbo.rest266")
public class ApplicationConfiguration {

    public static final String REDIS_ORDER_NAME = "order";
//
//    @Primary
//    @Bean("orderDataSource")
//    @ClubConfigurationProperties("club-boot.datasource.order")
//    public DataSource primaryDataSource() {
//        return ClubDataSourceFactory.create();
//    }
//
//    @Bean("odoo")
//    @RoutableStrategy(strategy = "parameter", param="zone", useDefault = true)
//    @ClubConfigurationProperties("club-boot.datasource.odoo")
//    public DataSource idoo() {
//        return RoutableDataSourceFactory.create();
//    }
//
//    @Bean("couponDataSource")
//    @ClubConfigurationProperties("club-boot.datasource.coupon")
//    public DataSource secondDataSource() {
//        return ClubDataSourceFactory.create();
//    }
//
//    @Primary
//    @Bean("orderRedisTemplate")
//    @ClubConfigurationProperties("club-boot.redis.order")
//    public StringRedisTemplate primaryRedis() {
//        return ClubRedisFactory.createStringTemplate();
//    }


    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customizer(){
        return new Jackson2ObjectMapperBuilderCustomizer(){

            @Override
            public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {

                SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("list");
                SimpleFilterProvider filterProvider = new SimpleFilterProvider().setDefaultFilter(filter);
                jacksonObjectMapperBuilder.filters(filterProvider);
            }
        };
    }
}
