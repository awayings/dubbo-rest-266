package com.clubfactory.platform.dubbo.rest266.server.provider;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleWriteDubboService;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleWriteService;
import com.clubfactory.platform.dubbo.rest266.core.dataobject.ExampleDO;
import com.clubfactory.platform.dubbo.rest266.server.handler.ProviderExceptionHandler;
import com.clubfactory.platform.dubborest.util.DubboRestUtil;
import com.clubfactory.platform.resttest.api.DemoService;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Service 用于SOA提供对外服务
 * Created by huage on 2018/8/6.
 */
@Service(version = "1.1")
@Slf4j
public class ExampleWriteDubboServiceImpl implements ExampleWriteDubboService {

    private BeanCopier beanCopier = BeanCopier.create(ExampleDTO.class, ExampleDO.class, false);

    private AtomicLong integer = new AtomicLong(0);

    @Reference(version = "1.1")
    DemoService demoService;

    @Override
    @RequestLine(value = "POST /feign/echoBody")
    public Result<ExampleDTO> save(ExampleDTO exampleDTO) {
        try {
            exampleDTO.setId(String.valueOf(integer.getAndIncrement()));
            return Result.of(exampleDTO);
        } catch(Exception e) {
            log.error("error", e);
            return ProviderExceptionHandler.processException(e);
        }
    }

    @Override
    @RequestLine(value = "GET /feign/echoPath/{fix}")
    public ExampleDTO publish(@Param(value = "id") Integer id, @Param(value = "fix") String path, @Param(value = "date2") String date, @Param(value = "name") String name){
        log.info("map2-3:{}", DubboRestUtil.getClientBasic());

//        String x = demoService.echoA("1", "");
//        String y = demoService.echoA("a", "a");

        ExampleDTO dto = new ExampleDTO();
        dto.setId("1.2");
        dto.setName(path + ":" + date + ":" + name);

        if (id != null && id % 5 == 0){
            throw new IllegalStateException("11");
        }

        if (id != null){
            try {
                Thread.sleep(id);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        dto.setLocalDate(LocalDate.now());
        return dto;
    }

    private ExampleDO mapping(ExampleDTO exampleDTO) {
        ExampleDO exampleDO = new ExampleDO();
        beanCopier.copy(exampleDTO, exampleDO, null);
        return exampleDO;
    }

}
