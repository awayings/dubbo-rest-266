package com.clubfactory.platform.dubbo.rest266.client.service;

import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by huage on 2018/8/9.
 */
public interface ExampleWriteDubboService {

    /**
     * 保存商品, 返回新的商品ID
     */
    Result<ExampleDTO> save(ExampleDTO productDO);

    /**
     * 发布商品
     */

    ExampleDTO publish(Integer id, String path, String date, String name);

}
