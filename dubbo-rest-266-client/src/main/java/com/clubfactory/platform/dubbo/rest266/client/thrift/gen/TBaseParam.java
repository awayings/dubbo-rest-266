/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.clubfactory.platform.dubbo.rest266.client.thrift.gen;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked", "unused"})
@javax.annotation.Generated(value = "Autogenerated by Thrift Compiler (0.12.0)", date = "2019-07-26")
public class TBaseParam implements org.apache.thrift.TBase<TBaseParam, TBaseParam._Fields>, java.io.Serializable, Cloneable, Comparable<TBaseParam> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("TBaseParam");

  private static final org.apache.thrift.protocol.TField USER_IDENTIFY_FIELD_DESC = new org.apache.thrift.protocol.TField("userIdentify", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField USER_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("userId", org.apache.thrift.protocol.TType.I64, (short)2);
  private static final org.apache.thrift.protocol.TField CART_OPERATE_ENUM_FIELD_DESC = new org.apache.thrift.protocol.TField("cartOperateEnum", org.apache.thrift.protocol.TType.I32, (short)3);

  private static final org.apache.thrift.scheme.SchemeFactory STANDARD_SCHEME_FACTORY = new TBaseParamStandardSchemeFactory();
  private static final org.apache.thrift.scheme.SchemeFactory TUPLE_SCHEME_FACTORY = new TBaseParamTupleSchemeFactory();

  public @org.apache.thrift.annotation.Nullable java.lang.String userIdentify; // optional
  public long userId; // optional
  /**
   * 
   * @see TCartOperateEnum
   */
  public @org.apache.thrift.annotation.Nullable TCartOperateEnum cartOperateEnum; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    USER_IDENTIFY((short)1, "userIdentify"),
    USER_ID((short)2, "userId"),
    /**
     * 
     * @see TCartOperateEnum
     */
    CART_OPERATE_ENUM((short)3, "cartOperateEnum");

    private static final java.util.Map<java.lang.String, _Fields> byName = new java.util.HashMap<java.lang.String, _Fields>();

    static {
      for (_Fields field : java.util.EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // USER_IDENTIFY
          return USER_IDENTIFY;
        case 2: // USER_ID
          return USER_ID;
        case 3: // CART_OPERATE_ENUM
          return CART_OPERATE_ENUM;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new java.lang.IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    @org.apache.thrift.annotation.Nullable
    public static _Fields findByName(java.lang.String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final java.lang.String _fieldName;

    _Fields(short thriftId, java.lang.String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public java.lang.String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __USERID_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  private static final _Fields optionals[] = {_Fields.USER_IDENTIFY,_Fields.USER_ID,_Fields.CART_OPERATE_ENUM};
  public static final java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    java.util.Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new java.util.EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.USER_IDENTIFY, new org.apache.thrift.meta_data.FieldMetaData("userIdentify", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.USER_ID, new org.apache.thrift.meta_data.FieldMetaData("userId", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.CART_OPERATE_ENUM, new org.apache.thrift.meta_data.FieldMetaData("cartOperateEnum", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.EnumMetaData(org.apache.thrift.protocol.TType.ENUM, TCartOperateEnum.class)));
    metaDataMap = java.util.Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(TBaseParam.class, metaDataMap);
  }

  public TBaseParam() {
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public TBaseParam(TBaseParam other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetUserIdentify()) {
      this.userIdentify = other.userIdentify;
    }
    this.userId = other.userId;
    if (other.isSetCartOperateEnum()) {
      this.cartOperateEnum = other.cartOperateEnum;
    }
  }

  public TBaseParam deepCopy() {
    return new TBaseParam(this);
  }

  @Override
  public void clear() {
    this.userIdentify = null;
    setUserIdIsSet(false);
    this.userId = 0;
    this.cartOperateEnum = null;
  }

  @org.apache.thrift.annotation.Nullable
  public java.lang.String getUserIdentify() {
    return this.userIdentify;
  }

  public TBaseParam setUserIdentify(@org.apache.thrift.annotation.Nullable java.lang.String userIdentify) {
    this.userIdentify = userIdentify;
    return this;
  }

  public void unsetUserIdentify() {
    this.userIdentify = null;
  }

  /** Returns true if field userIdentify is set (has been assigned a value) and false otherwise */
  public boolean isSetUserIdentify() {
    return this.userIdentify != null;
  }

  public void setUserIdentifyIsSet(boolean value) {
    if (!value) {
      this.userIdentify = null;
    }
  }

  public long getUserId() {
    return this.userId;
  }

  public TBaseParam setUserId(long userId) {
    this.userId = userId;
    setUserIdIsSet(true);
    return this;
  }

  public void unsetUserId() {
    __isset_bitfield = org.apache.thrift.EncodingUtils.clearBit(__isset_bitfield, __USERID_ISSET_ID);
  }

  /** Returns true if field userId is set (has been assigned a value) and false otherwise */
  public boolean isSetUserId() {
    return org.apache.thrift.EncodingUtils.testBit(__isset_bitfield, __USERID_ISSET_ID);
  }

  public void setUserIdIsSet(boolean value) {
    __isset_bitfield = org.apache.thrift.EncodingUtils.setBit(__isset_bitfield, __USERID_ISSET_ID, value);
  }

  /**
   * 
   * @see TCartOperateEnum
   */
  @org.apache.thrift.annotation.Nullable
  public TCartOperateEnum getCartOperateEnum() {
    return this.cartOperateEnum;
  }

  /**
   * 
   * @see TCartOperateEnum
   */
  public TBaseParam setCartOperateEnum(@org.apache.thrift.annotation.Nullable TCartOperateEnum cartOperateEnum) {
    this.cartOperateEnum = cartOperateEnum;
    return this;
  }

  public void unsetCartOperateEnum() {
    this.cartOperateEnum = null;
  }

  /** Returns true if field cartOperateEnum is set (has been assigned a value) and false otherwise */
  public boolean isSetCartOperateEnum() {
    return this.cartOperateEnum != null;
  }

  public void setCartOperateEnumIsSet(boolean value) {
    if (!value) {
      this.cartOperateEnum = null;
    }
  }

  public void setFieldValue(_Fields field, @org.apache.thrift.annotation.Nullable java.lang.Object value) {
    switch (field) {
    case USER_IDENTIFY:
      if (value == null) {
        unsetUserIdentify();
      } else {
        setUserIdentify((java.lang.String)value);
      }
      break;

    case USER_ID:
      if (value == null) {
        unsetUserId();
      } else {
        setUserId((java.lang.Long)value);
      }
      break;

    case CART_OPERATE_ENUM:
      if (value == null) {
        unsetCartOperateEnum();
      } else {
        setCartOperateEnum((TCartOperateEnum)value);
      }
      break;

    }
  }

  @org.apache.thrift.annotation.Nullable
  public java.lang.Object getFieldValue(_Fields field) {
    switch (field) {
    case USER_IDENTIFY:
      return getUserIdentify();

    case USER_ID:
      return getUserId();

    case CART_OPERATE_ENUM:
      return getCartOperateEnum();

    }
    throw new java.lang.IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new java.lang.IllegalArgumentException();
    }

    switch (field) {
    case USER_IDENTIFY:
      return isSetUserIdentify();
    case USER_ID:
      return isSetUserId();
    case CART_OPERATE_ENUM:
      return isSetCartOperateEnum();
    }
    throw new java.lang.IllegalStateException();
  }

  @Override
  public boolean equals(java.lang.Object that) {
    if (that == null)
      return false;
    if (that instanceof TBaseParam)
      return this.equals((TBaseParam)that);
    return false;
  }

  public boolean equals(TBaseParam that) {
    if (that == null)
      return false;
    if (this == that)
      return true;

    boolean this_present_userIdentify = true && this.isSetUserIdentify();
    boolean that_present_userIdentify = true && that.isSetUserIdentify();
    if (this_present_userIdentify || that_present_userIdentify) {
      if (!(this_present_userIdentify && that_present_userIdentify))
        return false;
      if (!this.userIdentify.equals(that.userIdentify))
        return false;
    }

    boolean this_present_userId = true && this.isSetUserId();
    boolean that_present_userId = true && that.isSetUserId();
    if (this_present_userId || that_present_userId) {
      if (!(this_present_userId && that_present_userId))
        return false;
      if (this.userId != that.userId)
        return false;
    }

    boolean this_present_cartOperateEnum = true && this.isSetCartOperateEnum();
    boolean that_present_cartOperateEnum = true && that.isSetCartOperateEnum();
    if (this_present_cartOperateEnum || that_present_cartOperateEnum) {
      if (!(this_present_cartOperateEnum && that_present_cartOperateEnum))
        return false;
      if (!this.cartOperateEnum.equals(that.cartOperateEnum))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int hashCode = 1;

    hashCode = hashCode * 8191 + ((isSetUserIdentify()) ? 131071 : 524287);
    if (isSetUserIdentify())
      hashCode = hashCode * 8191 + userIdentify.hashCode();

    hashCode = hashCode * 8191 + ((isSetUserId()) ? 131071 : 524287);
    if (isSetUserId())
      hashCode = hashCode * 8191 + org.apache.thrift.TBaseHelper.hashCode(userId);

    hashCode = hashCode * 8191 + ((isSetCartOperateEnum()) ? 131071 : 524287);
    if (isSetCartOperateEnum())
      hashCode = hashCode * 8191 + cartOperateEnum.getValue();

    return hashCode;
  }

  @Override
  public int compareTo(TBaseParam other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = java.lang.Boolean.valueOf(isSetUserIdentify()).compareTo(other.isSetUserIdentify());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUserIdentify()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.userIdentify, other.userIdentify);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.valueOf(isSetUserId()).compareTo(other.isSetUserId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUserId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.userId, other.userId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = java.lang.Boolean.valueOf(isSetCartOperateEnum()).compareTo(other.isSetCartOperateEnum());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCartOperateEnum()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.cartOperateEnum, other.cartOperateEnum);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  @org.apache.thrift.annotation.Nullable
  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    scheme(iprot).read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    scheme(oprot).write(oprot, this);
  }

  @Override
  public java.lang.String toString() {
    java.lang.StringBuilder sb = new java.lang.StringBuilder("TBaseParam(");
    boolean first = true;

    if (isSetUserIdentify()) {
      sb.append("userIdentify:");
      if (this.userIdentify == null) {
        sb.append("null");
      } else {
        sb.append(this.userIdentify);
      }
      first = false;
    }
    if (isSetUserId()) {
      if (!first) sb.append(", ");
      sb.append("userId:");
      sb.append(this.userId);
      first = false;
    }
    if (isSetCartOperateEnum()) {
      if (!first) sb.append(", ");
      sb.append("cartOperateEnum:");
      if (this.cartOperateEnum == null) {
        sb.append("null");
      } else {
        sb.append(this.cartOperateEnum);
      }
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class TBaseParamStandardSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    public TBaseParamStandardScheme getScheme() {
      return new TBaseParamStandardScheme();
    }
  }

  private static class TBaseParamStandardScheme extends org.apache.thrift.scheme.StandardScheme<TBaseParam> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, TBaseParam struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // USER_IDENTIFY
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.userIdentify = iprot.readString();
              struct.setUserIdentifyIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // USER_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.userId = iprot.readI64();
              struct.setUserIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // CART_OPERATE_ENUM
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.cartOperateEnum = com.clubfactory.platform.dubbo.rest266.client.thrift.gen.TCartOperateEnum.findByValue(iprot.readI32());
              struct.setCartOperateEnumIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, TBaseParam struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.userIdentify != null) {
        if (struct.isSetUserIdentify()) {
          oprot.writeFieldBegin(USER_IDENTIFY_FIELD_DESC);
          oprot.writeString(struct.userIdentify);
          oprot.writeFieldEnd();
        }
      }
      if (struct.isSetUserId()) {
        oprot.writeFieldBegin(USER_ID_FIELD_DESC);
        oprot.writeI64(struct.userId);
        oprot.writeFieldEnd();
      }
      if (struct.cartOperateEnum != null) {
        if (struct.isSetCartOperateEnum()) {
          oprot.writeFieldBegin(CART_OPERATE_ENUM_FIELD_DESC);
          oprot.writeI32(struct.cartOperateEnum.getValue());
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class TBaseParamTupleSchemeFactory implements org.apache.thrift.scheme.SchemeFactory {
    public TBaseParamTupleScheme getScheme() {
      return new TBaseParamTupleScheme();
    }
  }

  private static class TBaseParamTupleScheme extends org.apache.thrift.scheme.TupleScheme<TBaseParam> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, TBaseParam struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol oprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      java.util.BitSet optionals = new java.util.BitSet();
      if (struct.isSetUserIdentify()) {
        optionals.set(0);
      }
      if (struct.isSetUserId()) {
        optionals.set(1);
      }
      if (struct.isSetCartOperateEnum()) {
        optionals.set(2);
      }
      oprot.writeBitSet(optionals, 3);
      if (struct.isSetUserIdentify()) {
        oprot.writeString(struct.userIdentify);
      }
      if (struct.isSetUserId()) {
        oprot.writeI64(struct.userId);
      }
      if (struct.isSetCartOperateEnum()) {
        oprot.writeI32(struct.cartOperateEnum.getValue());
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, TBaseParam struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TTupleProtocol iprot = (org.apache.thrift.protocol.TTupleProtocol) prot;
      java.util.BitSet incoming = iprot.readBitSet(3);
      if (incoming.get(0)) {
        struct.userIdentify = iprot.readString();
        struct.setUserIdentifyIsSet(true);
      }
      if (incoming.get(1)) {
        struct.userId = iprot.readI64();
        struct.setUserIdIsSet(true);
      }
      if (incoming.get(2)) {
        struct.cartOperateEnum = com.clubfactory.platform.dubbo.rest266.client.thrift.gen.TCartOperateEnum.findByValue(iprot.readI32());
        struct.setCartOperateEnumIsSet(true);
      }
    }
  }

  private static <S extends org.apache.thrift.scheme.IScheme> S scheme(org.apache.thrift.protocol.TProtocol proto) {
    return (org.apache.thrift.scheme.StandardScheme.class.equals(proto.getScheme()) ? STANDARD_SCHEME_FACTORY : TUPLE_SCHEME_FACTORY).getScheme();
  }
}

