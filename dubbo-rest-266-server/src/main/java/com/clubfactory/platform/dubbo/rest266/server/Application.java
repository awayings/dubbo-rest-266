package com.clubfactory.platform.dubbo.rest266.server;

import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleWriteService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"com.clubfactory.platform.dubbo.rest266", "com.clubfactory.center.dubbothrift"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}


//	@Bean
	ApplicationRunner run(){

		return new ApplicationRunner() {

//			@Reference(registry = "remote")
			ExampleWriteService exampleWriteService;

			@Override
			public void run(ApplicationArguments args) throws Exception {

				new Thread(() ->{

					try {
						Thread.sleep(3000);
						System.out.println("start run v284. ");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					for (int i = 0; i < 100; i++){

						try{
							Result t = exampleWriteService.save(new ExampleDTO());
							System.out.println("ret: v284" + t);

							Thread.sleep(1000);
						}catch (Exception e){
							e.printStackTrace();
						}

					}

				}).start();

			}
		};

	}
}
