package com.clubfactory.platform.dubbo.rest266.server.handler;

import com.clubfactory.boot.client.exception.ClubBootBaseException;
import com.clubfactory.boot.client.exception.CommonCodes;
import com.clubfactory.boot.client.result.Result;

/**
 * @author lianghaijun
 * @date 2018/9/19
 */
public class ProviderExceptionHandler {

    public static <T> Result<T> processException(Exception e){
        if (e instanceof ClubBootBaseException){
            return Result.failure(((ClubBootBaseException) e).getCodeMessage());
        }
        return Result.failure(CommonCodes.SYSTEM_ERROR.toCodeMessage());
    }

}
