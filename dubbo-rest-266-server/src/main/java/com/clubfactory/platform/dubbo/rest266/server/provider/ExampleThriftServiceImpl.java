package com.clubfactory.platform.dubbo.rest266.server.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.clubfactory.platform.dubbo.rest266.client.thrift.gen.ExampleThriftService;
import com.clubfactory.platform.dubbo.rest266.client.thrift.gen.TBaseParam;
import com.clubfactory.platform.dubbo.rest266.client.thrift.gen.TCartOperateEnum;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TException;

import java.util.List;
import java.util.Map;

/**
 * @author jingzhang@clubfactory.com
 * @time 2018-2-28 09:13:52
 */
@Service(protocol = {"service1", "dubbo"})
@Slf4j
public class ExampleThriftServiceImpl implements ExampleThriftService.Iface {

    @Override
    public String echo(String path) throws TException {

        if (StringUtils.isNumeric(path)){
            int i = Integer.parseInt(path);
            try {
                Thread.sleep(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        log.info("sleeped. {}", path);
        return path;
    }

    @Override
    public String getResult(TBaseParam param) throws TException {
        log.info("data:{}", param);
        return JSON.toJSONString(param);
    }

    @Override
    public Map<String, List<TBaseParam>> mapResult() throws TException {

        Map<String, List<TBaseParam>> map = Maps.newHashMap();

        TBaseParam param = new TBaseParam();
        param.setCartOperateEnum(TCartOperateEnum.MERGE);
        param.setUserIdentify("uid");

        map.put("take", Lists.newArrayList(param));
        return map;
    }

    @Override
    public List<TBaseParam> listResult() throws TException {

        TBaseParam param = new TBaseParam();
        param.setCartOperateEnum(TCartOperateEnum.BATCH_DELETE);
        param.setUserIdentify("uid2");

        return Lists.newArrayList(param);
    }

    @Override
    public List<Map<String, TBaseParam>> listMapResult() throws TException {

        TBaseParam param = new TBaseParam();
        param.setCartOperateEnum(TCartOperateEnum.BATCH_DELETE);
        param.setUserIdentify("uid3");

        Map<String, TBaseParam> ret = Maps.newHashMap();
        ret.put("like", param);

        return Lists.newArrayList(ret);
    }
}
