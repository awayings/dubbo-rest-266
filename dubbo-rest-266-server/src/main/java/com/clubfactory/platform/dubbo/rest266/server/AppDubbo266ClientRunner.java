package com.clubfactory.platform.dubbo.rest266.server;

import com.alibaba.dubbo.config.annotation.Reference;
import com.clubfactory.boot.client.result.Result;

import com.clubfactory.boot.util.HttpUtil;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleWriteService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author jingzhang@clubfactory.com
 * @time 2018-2-28 09:13:52
 */
//@Component
public class AppDubbo266ClientRunner implements ApplicationRunner {

    @Reference
    ExampleWriteService exampleWriteService;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        new Thread(() ->{

            try {
                Thread.sleep(3000);
                System.out.println("start run v266. ");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < 100; i++){

                try{
                    Result t = exampleWriteService.save(new ExampleDTO());
                    System.out.println("ret v266:" + t);

                    Thread.sleep(1000);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

        }).start();
    }

}
