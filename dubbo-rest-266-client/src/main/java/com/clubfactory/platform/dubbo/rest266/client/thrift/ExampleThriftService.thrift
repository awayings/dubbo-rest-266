namespace java com.clubfactory.platform.dubbo.rest266.client.thrift.gen
namespace py com.clubfactory.platform.dubbo.rest266.client.thrift.gen
enum TCartOperateEnum {
  USER_ADD = 1
  USER_UPDATE = 2
  USER_DELETE = 3
  BATCH_DELETE = 4
  BATCH_GET = 5
  MERGE = 6
}
struct TBaseParam{
    1:optional string userIdentify
    2:optional i64 userId
    3:optional TCartOperateEnum cartOperateEnum
}

service ExampleThriftService{
    string echo(1:string path)
    string getResult(1:TBaseParam param)
    map<string, list<TBaseParam>> mapResult()
    list<TBaseParam> listResult()
    list<map<string, TBaseParam>> listMapResult()
}

