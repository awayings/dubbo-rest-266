package com.clubfactory.platform.dubbo.rest266.client.exception;

import com.clubfactory.boot.client.exception.ClubBootBaseException;
import com.clubfactory.boot.client.exception.CommonCodes;

/**
 * @author jingzhang@clubfactory.com
 * @time 2018-2-28 09:13:52
 */
public class UserCaseCodes extends CommonCodes {

    public static final CodeMessageWrapper<ClubBootBaseException> BUSINESS_ERROR = CodeMessageWrapper.of(110001, "业务异常", ClubBootBaseException::new);

}
