package com.clubfactory.platform.dubbo.rest266.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;


@SuppressWarnings("SpringJavaAutoWiringInspection")
@Configuration
//@MapperScan(value = "com.clubfactory.platform.dubbo.rest266.core.dao")
//@EnableTransactionManagement
@Slf4j
public class MybatisConfig {
//
//    @Autowired
//    @Qualifier("couponDataSource")
//    private DataSource dataSource;
//
//    @Bean
//    public SqlSessionFactory sqlSessionFactories() throws Exception {
//        log.info("------------- 重载父类 sqlSessionFactory init-------");
//        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
//        factory.setDataSource(dataSource);
//
//        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//        factory.setMapperLocations(resolver.getResources("classpath:/mapper/*Mapper.xml"));
//        return factory.getObject();
//    }
}
