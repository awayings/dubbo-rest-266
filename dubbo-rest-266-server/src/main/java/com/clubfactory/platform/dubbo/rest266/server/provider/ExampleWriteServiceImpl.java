package com.clubfactory.platform.dubbo.rest266.server.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleWriteService;
import com.clubfactory.platform.dubbo.rest266.core.dataobject.ExampleDO;
import com.clubfactory.platform.dubbo.rest266.server.handler.ProviderExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.web.bind.annotation.CookieValue;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Service 用于SOA提供对外服务
 * Created by huage on 2018/8/6.
 */
@Service
@Slf4j
public class ExampleWriteServiceImpl implements ExampleWriteService {


    private BeanCopier beanCopier = BeanCopier.create(ExampleDTO.class, ExampleDO.class, false);

    private AtomicLong integer = new AtomicLong(0);

    @Override
    public Result<Long> save(ExampleDTO exampleDTO) {
        try {
            ExampleDO exampleDO = mapping(exampleDTO);
            return Result.of(integer.getAndIncrement());
        } catch(Exception e) {
            log.error("error", e);
            return ProviderExceptionHandler.processException(e);
        }
    }

    @Override
    public Result<Void> publish(ExampleDTO exampleDTO) {
        try {
            ExampleDO exampleDO = mapping(exampleDTO);
            return Result.success();
        } catch(Exception e) {
            return ProviderExceptionHandler.processException(e);
        }
    }

    @Override
    public Result<String> mutiParam(List<ExampleDTO> list, ExampleDTO[] array, Map<String, ExampleDTO> map, Integer count) {

        log.info("x:{}", list);

        log.info("x:{}", array);

        log.info("x:{}", map);

        return Result.of("true");
    }


    private ExampleDO mapping(ExampleDTO exampleDTO) {
        ExampleDO exampleDO = new ExampleDO();
        beanCopier.copy(exampleDTO, exampleDO, null);
//        switch(exampleDTO.getStatus()) {
//            case SAVED:
//                exampleDO.setStatus(ExampleStatus.SAVED);
//                break;
//            case PUBLISH:
//                exampleDO.setStatus(ExampleStatus.PUBLISH);
//                break;
//            case DELETED:
//                exampleDO.setStatus(ExampleStatus.DELETED);
//                break;
//        }
        return exampleDO;
    }

}
