package com.clubfactory.platform.dubbo.rest266.server.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author lianghaijun
 * @date 2018-12-04
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    @ResponseBody
    public String checkHealth() {
        return "<a href=/datasource>datasource</a><br><a href=/redis>redis</a>";
    }


    @Data
    static class A{
        int a;
        String b;
        Date date;
        Map<Integer, Integer> map;
        List<Map<Integer, Integer>> list;
        B b1;
    }

    @Data
    static class B{
        int b1;
        A a;
    }

    @Resource
    ObjectMapper objectMapper;

    @RequestMapping("/t1")
    @ResponseBody
    public A test() {
        A a = new A();
        a.setDate(new Date());

        Map<Integer, Integer> map1 = Maps.newHashMap();
        map1.putIfAbsent(1, 1);
        a.setMap(map1);

        return a;
    }

    @RequestMapping("/t2")
    @ResponseBody
    public String test2(@RequestParam(value = "key", required = false) Integer key) throws JsonProcessingException {
        A a = new A();
        a.setDate(new Date());

        Map<Integer, Integer> map1 = Maps.newHashMap();
        map1.putIfAbsent(1, 1);
        a.setMap(map1);

        a.setList(Lists.newArrayList(map1));

//        B b = new B();
//        b.setA(a);
//        a.setB1(b);

        String ret = null;
        if (key == null) {
            ret = objectMapper.writeValueAsString(a);
        }else if(key == 1) {
            ret = JSON.toJSONString(a, SerializerFeature.DisableCircularReferenceDetect);
        }else if (key == 2){
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            ret = mapper.writeValueAsString(a);
        }

        return ret;
    }

}
