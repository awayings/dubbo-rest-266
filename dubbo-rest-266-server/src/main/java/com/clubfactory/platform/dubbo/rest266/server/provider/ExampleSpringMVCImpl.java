package com.clubfactory.platform.dubbo.rest266.server.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleSpringMVCService;
import com.clubfactory.platform.dubbo.rest266.client.service.ExampleWriteService;
import com.clubfactory.platform.dubbo.rest266.core.dataobject.ExampleDO;
import com.clubfactory.platform.dubbo.rest266.server.handler.ProviderExceptionHandler;
import com.clubfactory.platform.dubborest.metadata.resolver.SpringQueryMap;
import com.clubfactory.platform.dubborest.util.ClientBasic;
import com.clubfactory.platform.dubborest.util.DubboRestUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Service 用于SOA提供对外服务
 * Created by huage on 2018/8/6.
 */
@Service
@Slf4j
public class ExampleSpringMVCImpl implements ExampleSpringMVCService {

    @Override
    @RequestMapping(value = "/spring/echo/{abc}", method = RequestMethod.GET)
    public String echo(@RequestParam String name,
                       @RequestHeader(value = "client-basic")String clientBasic,
                       @PathVariable String abc,
                       @CookieValue("uid") String uid,
                       @SpringQueryMap ExampleDTO exampleDTO // 使用对象接收queryString
    ) {

        Map<String, Object> ret = Maps.newHashMap();
        ret.put("Client-Basic", clientBasic);
        ret.put("client-basic-map", DubboRestUtil.getClientBasicMap());
        ret.put("client-basic-obj", DubboRestUtil.getClientBasic());
        ret.put("cookie", DubboRestUtil.getCookies().toString());

        Map<String, String> headers = Maps.newHashMap();
        headers.putIfAbsent("Set-Cookie", "abc=" + name + "; Path=/");

        DubboRestUtil.customRestResult(200, headers);
        return JSON.toJSONString(ret);
    }

    @Override
    @RequestMapping(value = "/spring/body", method = RequestMethod.POST)
    public String bodyTest(@RequestBody List<ExampleDTO> list) {

        return JSON.toJSONString(list);
    }


    @Override
    @RequestMapping(value = "/spring/body1", method = RequestMethod.POST)
    public String bodyTestDto(@RequestBody ExampleDTO dto) {
        return JSON.toJSONString(dto);
    }
}
