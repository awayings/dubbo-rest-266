package com.clubfactory.platform.dubbo.rest266.client.service;

import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;

import java.util.List;

/**
 * Created by huage on 2018/8/9.
 */
public interface ExampleSpringMVCService {
    public String echo(String name, String clientBasic,String abc, String uid, ExampleDTO exampleDTO);
    public String bodyTest(List<ExampleDTO> list);
    public String bodyTestDto(ExampleDTO dto);

}
