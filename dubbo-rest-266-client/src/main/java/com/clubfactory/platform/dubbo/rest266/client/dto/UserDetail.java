package com.clubfactory.platform.dubbo.rest266.client.dto;

import lombok.Data;

import java.io.Serializable;
import java.text.MessageFormat;

/**
 * @author jingzhang@clubfactory.com
 * @time 2018-2-28 09:13:52
 */
@Data
public class UserDetail extends User {

    String name;

    String firstName;
}
