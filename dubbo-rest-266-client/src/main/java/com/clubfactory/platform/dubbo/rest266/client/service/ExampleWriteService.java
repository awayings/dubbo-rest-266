package com.clubfactory.platform.dubbo.rest266.client.service;

import com.clubfactory.boot.client.result.Result;
import com.clubfactory.platform.dubbo.rest266.client.dto.ExampleDTO;

import java.util.List;
import java.util.Map;

/**
 * Created by huage on 2018/8/9.
 */
public interface ExampleWriteService {

    /**
     * 保存商品, 返回新的商品ID
     */
    public Result<Long> save(ExampleDTO productDO);

    /**
     * 发布商品
     */
    public Result<Void> publish(ExampleDTO productDO);

    /**
     * 测试混合场景。
     *
     * @param list
     * @param count
     * @return
     */
    public Result<String> mutiParam(List<ExampleDTO> list, ExampleDTO[] array, Map<String, ExampleDTO> map, Integer count);
}
