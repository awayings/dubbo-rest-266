package com.clubfactory.platform.dubbo.rest266.client.dto;

import com.clubfactory.platform.dubbo.rest266.client.enums.ExampleDTOStatus;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by huage on 2018/8/6.
 */
@Data
public class ExampleDTO implements Serializable{

    private String id;

    private String name;

    private ExampleDTOStatus status;

    private LocalDate localDate;

    private List<? extends User> userList;

    private User user;
}
